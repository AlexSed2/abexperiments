import XCTest
@testable import Experiments

final class ExperimentsTests: XCTestCase {
    
    
    func testAPI_1() {
        try! DiskStorage().hardReset()
        let now = try! QuickDate(string: "01.05.1000").date
        let installDate =  try! QuickDate(string: "01.02.1000").date
        let library = ExperimentsLibrary(installDate: installDate, testNow: now) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.01.1000", duration: 60)
            }

        } callBack: { _ in }
        XCTAssert(library.timeline.superpProperty.isEmpty)
        XCTAssert(library.timeline.allHaveBeenActive.isEmpty)
    }
    
    func testAPI_2() {
        try! DiskStorage().hardReset()
        let now = try! QuickDate(string: "01.03.1000").date
        let installDate =  try! QuickDate(string: "01.02.1000").date
        let library = ExperimentsLibrary(installDate: installDate, testNow: now) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.01.1000", duration: 60)
            }

        } callBack: { _ in }
        XCTAssert(library.timeline.superpProperty.isEmpty)
        XCTAssert(library.timeline.allHaveBeenActive.isEmpty)
    }
    
    func testAPI_3() {
        try! DiskStorage().hardReset()
        let now = try! QuickDate(string: "01.03.1000").date
        let installDate =  try! QuickDate(string: "01.01.1000").date
        let library = ExperimentsLibrary(installDate: installDate, testNow: now) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.02.1000", duration: 60)
            }

        } callBack: { _ in }
        XCTAssert(library.timeline.superpProperty.isEmpty == false)
        XCTAssert(library.timeline.allHaveBeenActive.isEmpty == false)
    }
    
    func testAPI_4() {
        try! DiskStorage().hardReset()
        let now = try! QuickDate(string: "01.07.1000").date
        let installDate =  try! QuickDate(string: "01.01.1000").date
        let library = ExperimentsLibrary(installDate: installDate, testNow: now) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.02.1000", duration: 60)
            }

        } callBack: { _ in }
        XCTAssert(library.timeline.superpProperty.isEmpty == true)
        XCTAssert(library.timeline.allHaveBeenActive.isEmpty == false)
    }
    
    func testDates() {
        let start = try! QuickDate(string: "01.01.1000")
        let end = try! QuickDate(string: "10.01.1000")
        let dates = Dates(startDate: start, endDate: end)
        print("dates: \(dates)")
        var now = try! QuickDate(string: "05.01.1000").date
        XCTAssert(dates.contains(now))
        now = try! QuickDate(string: "15.01.1000").date
        XCTAssert(dates.contains(now) == false)
    }
    
    func testEssense() throws {
        try! DiskStorage().hardReset()
        let now = try! QuickDate(string: "10.01.1000").date
        let library = ExperimentsLibrary(testNow: now) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.01.1000", duration: 30)
            }
            Group(name: "Beta", sample: 0) {
                Experiment(name: "3", startDate: "01.01.1000", duration: 30)
            }
        } callBack: { _ in }
        
        
        XCTAssert(library.timeline.isActive(nameExperiment: "1") == true)
        XCTAssert(library.timeline.isActive(nameExperiment: "3") == false)
        XCTAssert(library.timeline.isActive(nameExperiment: "4") == false)
        
        let serverGroups = Groups {
            Group(name: "Delta", sample: 0) {
                Experiment(name: "1", startDate: "01.01.1000", duration: 30)
            }
            Group(name: "Beta", sample: 100) {
                Experiment(name: "3", startDate: "01.01.1000", duration: 30)
            }
        }
        
        library.set(serverExperiments: serverGroups.groups)
        
        XCTAssert(library.timeline.isActive(nameExperiment: "1") == false)
        XCTAssert(library.timeline.isActive(nameExperiment: "3") == true)
        XCTAssert(library.timeline.isActive(nameExperiment: "4") == false)
        
    }
}
