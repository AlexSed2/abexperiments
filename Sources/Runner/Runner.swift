import Experiments

@main struct Runner {
    static func main() {
        Runner().test()
    }
    func test() {
        Test().test()
    }
    func libraryUse() {
        
        let _ = ExperimentsLibrary(installDate: .distantPast) {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "21.01.2022", duration: 10)
                Experiment(name: "2", startDate: "21.03.2022", duration: 10)
            }
            Group(name: "Beta", sample: 100) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        } callBack: { packet in
            print("AB: userArray: \(packet.user.map(\.name))")
            print("AB: superPropertyArray: \(packet.superProperty.map(\.name))")
        }
        
    }
}
