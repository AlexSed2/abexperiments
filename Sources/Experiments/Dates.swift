/*
 Dates.swift
 */

import Foundation

struct DateError: Error {}

// Инициализируем по UTC: 00:00
struct QuickDate: Equatable {
    let date: Date
    /// - Осторожней с 31 числом, не во всех месяцах оно есть
    init(day: Int, month: Int, year: Int) throws {
        guard (0...31).contains(day) else { throw DateError() }
        guard (0...12).contains(month) else { throw DateError() }
        let string = "\(String(day)).\(String(month)).\(String(year))"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        guard let date = dateFormatter.date(from: string) else { throw DateError() }
        self.date = Calendar.current.startOfDay(for: date)
    }
    /// 02.12.2022
    /// - Осторожней с 31 числом, не во всех месяцах оно есть
    init(string: String) throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        guard let date = dateFormatter.date(from: string) else { throw DateError() }
        self.date = Calendar.current.startOfDay(for: date)
    }
}

struct Dates: Equatable, Hashable, Codable {
    
    let start: Date
    let end: Date
    
    init(startDate: QuickDate, duration: Int) {
        start = startDate.date
        end = Calendar.current.date(byAdding: .day, value: duration, to: startDate.date)!
    }
    init(startDate: Date, duration: Int) {
        start = startDate
        end = Calendar.current.date(byAdding: .day, value: duration, to: startDate)!
    }
    init(startDate: QuickDate, endDate: QuickDate) {
        start = startDate.date
        end = endDate.date
    }
    func contains(_ date: Date) -> Bool {
        (start...end).contains(date)
    }
}
