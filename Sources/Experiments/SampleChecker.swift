/*
 SampleChecker.swift
 Для расчёта, попадёт ли юзер в контрольную группу данной группы экспериментов
 или в активную(Будет ли учавствовать в экспериментах, котроые предлагает группа).
 Важно: запускается только один раз для любой группы экспериментов,
 в дальнейшем значение берётся с диска.
 */

struct SampleChecker {
    
    private let hundredPercentNumber = 100.0
    
    /// 0...1
    func isInside(sizeInFraction: Double) -> Bool {
        isInside(sizeInPercent: sizeInFraction * 100)
    }
    /// 0...100
    func isInside(sizeInPercent: Double) -> Bool {
        if sizeInPercent < 0 {
            return false
        }
        
        if sizeInPercent > hundredPercentNumber {
            return true
        }
        let randomNumber = hundredPercentNumber - Double.random(in: 0...hundredPercentNumber)
        let value = hundredPercentNumber - randomNumber
        let result = value <= sizeInPercent
        return result
    }
}
