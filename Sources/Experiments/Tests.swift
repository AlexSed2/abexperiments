struct TestIncomServerExperiments {
    func test() {
 
        let library = ExperimentsLibrary {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "01.01.2022", duration: 100)
                Experiment(name: "2", startDate: "01.01.2022", duration: 100)
            }
            Group(name: "Beta", sample: 100) {
                Experiment(name: "3", startDate: "01.01.2022", duration: 100)
                Experiment(name: "4", startDate: "01.03.2022", duration: 100)
            }
        } callBack: { _ in }
        
        
        let serverGroups = Groups {
            Group(name: "Delta", sample: 100) {
                Experiment(name: "1", startDate: "10.10.3000", duration: 100)
                Experiment(name: "2", startDate: "10.10.3000", duration: 100)
            }
        }
        library.set(serverExperiments: serverGroups.groups)
        
        print(library.timeline)
        print(library.timeline.current)
    }
}

struct TestGroupSetup {
    func testInitialisation() {
        try! DiskStorage().hardReset()
        let _ = ExperimentsLibrary {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "21.01.2022", duration: 10)
                Experiment(name: "2", startDate: "21.03.2022", duration: 10)
            }
            Group(name: "Beta", sample: 100) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        } callBack: { _ in }
        
        let new = Groups {
            Group(name: "Alpha", sample: 0) {
                Experiment(name: "5", startDate: "21.01.2022", duration: 10)
                Experiment(name: "6", startDate: "21.03.2022", duration: 10)
            }
            Group(name: "Gamma", sample: 0) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        }
        
        let storedGroups = try! DiskStorage().load(target: .device)
        let setuped = GroupSetup().setupGroups(incomGroups: new, storedGroups: storedGroups)
        assert(setuped.groups.contains { $0.name == "Alpha" })
        assert(setuped.groups.contains { $0.name == "Beta" })
        assert( setuped.groups.contains { $0.name == "Gamma" })
        print("Compleet")
        print(setuped)
    }
}

struct TestExperiments {
    
    func create() {
        let _ = ExperimentsLibrary {
            Group(name: "Alpha", sample: 100) {
                Experiment(name: "1", startDate: "21.01.2022", duration: 10)
                Experiment(name: "2", startDate: "21.03.2022", duration: 10)
            }
            Group(name: "Beta", sample: 100) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        } callBack: { _ in }
    }
    
    func test() {
        create()
    }
}

struct TestGroups {
    func makeGroups() -> Groups {
        Groups {
            Group(name: "Alpha", sample: 0) {
                Experiment(name: "1", startDate: "21.01.2022", duration: 10)
                Experiment(name: "2", startDate: "21.03.2022", duration: 10)
            }
            Group(name: "Beta", sample: 0) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        }
    }
    func makeLittleGroups() -> Groups {
        Groups {
            Group(name: "Beta", sample: 0) {
                Experiment(name: "3", startDate: "21.01.2022", duration: 10)
                Experiment(name: "4", startDate: "21.03.2022", duration: 10)
            }
        }
    }
    func groupDiff() {
        let big = makeGroups()
        let little = makeLittleGroups()
        print(big)
        print(little)
        let diff = big - little
        print("diff")
        print(diff)
    }
    func test() {
        groupDiff()
    }
}

struct TestStorage {
    func test() {
        let storage = DiskStorage()
        let model = TestGroups().makeGroups()
        try! storage.save(model: model, target: .device)
    }
}

struct TestDates {
    func makeDates() {
        let date = try! QuickDate(day: 31, month: 10, year: 2022)
        let date2 = try! QuickDate(string: "31.10.2022")
        assert(date == date2)
        print("Compleet")
    }
    func testRange() {
        let date = try! QuickDate(day: 31, month: 8, year: 2022)
        let date2 = try! QuickDate(string: "20.10.2022")
        let range = Dates(startDate: date, endDate: date2)
        let date3 = try! QuickDate(string: "20.09.2022")
        assert(range.contains(date3.date))
        let date4 = try! QuickDate(string: "20.01.2022")
        assert(range.contains(date4.date) == false)
        print("Compleet")
    }
    func test() {
        makeDates()
        testRange()
    }
}

public struct Test {
    
    public init() {}
    
    public func finalTest() {
        TestGroupSetup().testInitialisation()
        TestDates().test()
    }
    
    public func test() {
        TestIncomServerExperiments().test()
    }
    
}

// Forum Question
// https://forums.swift.org/t/result-builders-and-throws/54608

import Foundation

struct DateFormatError: Error {}

struct Day {
    let event: String
    let date: Date
    init(event: String, textDate: String) throws {
        self.event = event
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        guard let date = dateFormatter.date(from: textDate) else { throw DateFormatError() }
        self.date = Calendar.current.startOfDay(for: date)
    }
}

struct Week {
    let days: [Day]
    init(@WeekBuilder _ builder: () -> [Day]) { // how to make that initialiser 'throws' ?
        self.days = builder()
    }
}

@resultBuilder
struct WeekBuilder {
    static func buildBlock(_ components: (date: String, event: String)...) throws -> [Day] {
        
        let array = try components.map { try Day(event: $0.date, textDate: $0.event) }
        return array
        
    }
    
}

// Using
//struct WeekTest {
//    func test() {
//        do {
//            let week = try Week {
//                (date: "12.11.2022", event: "one")
//                (date: "13.11.2022", event: "one")
//            }
//        } catch {
//
//        }
//
//    }
//}
