/*
 Setup.swift
 */

struct Setup {
    
    // Изначальное заполнение экспериментов юзером
    // Нужно на initial передавать разность между тем что есть сейчас на диске и входящем от юзера массивом
    
    func fullfill(groups initial: Groups) -> Groups {
        var filled = initial
        filled.groups = Set(initial.groups.map { group in
            var group = group
            if SampleChecker().isInside(sizeInFraction: group.sample) {
                let user = User()
                group.user = user
                if var random = group.experiments.randomElement() {
                    random.user = user
                    group.experiments.update(with: random)
                }
            }
            return group
        })
        return filled
    }
    
}
