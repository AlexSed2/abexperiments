/*
 Experiment.swift
 Эксперимент. Поле 'user' заполнено, если данное устройство участвует в эксперименте.
 */

import Foundation

public struct Experiment: Equatable, Hashable, Codable {
    var user: User?
    public let name: String
    let dates: Dates
    
    init(name: String, dates: Dates) {
        self.user = nil
        self.name = name
        self.dates = dates
    }
    
    public init(name: String, from: String, to: String) {
        self.user = nil
        self.name = name
        self.dates = try! Dates(startDate: QuickDate(string: from), endDate: QuickDate(string: to))
    }
    public init(name: String, startDate: String, duration: Int) {
        self.user = nil
        self.name = name
        self.dates = try! Dates(startDate: QuickDate(string: startDate), duration: duration)
    }
    
    public init(name: String, startDate: Date, duration: Int) {
        self.user = nil
        self.name = name
        self.dates = Dates(startDate: startDate, duration: duration)
    }
    
    public static func == (lhs: Experiment, rhs: Experiment) -> Bool {
        lhs.name == rhs.name
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}

public struct User: Codable {
    var user = "👨🏻‍🦰"
}
