/*
 GroupShape.swift
 Эта структура отвечает за склейку груп, приходящих от юзера, и групп, хранящихся на диске
 */

struct GroupSetup {
    
    /*
     Принимаем группы от юзера, сетапим и сохраняем
     */
    func setupGroups(incomGroups: Groups, storedGroups: Groups) -> Groups {
        let tosetup = incomGroups - storedGroups
        let setuped = Setup().fullfill(groups: tosetup)
        let tosave = storedGroups + setuped
        return tosave
    }

}
