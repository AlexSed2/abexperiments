extension Experiment: CustomStringConvertible {
    public var description: String {
        "name: \(name), user: \(user?.user ?? "-"), dates: \(dates)"
    }
}

extension Timeline: CustomStringConvertible {
    var description: String {
        """
        user:
        \(userstring)
        
        server:
        \(serverstring)
        
        experiments:
        \(experimentsstring)
        ~
        """
    }
    
    var userstring: String {
        user.reduce(into: "") {
            $0 += "\($1)"
            $0 += "\n"
        }
    }
    
    var serverstring: String {
        guard let server = server else { return "" }
        return server.reduce(into: "") {
            $0 += "\($1)"
            $0 += "\n"
        }
    }
    
    var experimentsstring: String {
        experiments.reduce(into: "") {
            $0 += "\($1)"
            $0 += "\n"
        }
    }
    
}

extension Group: CustomStringConvertible {
    public var description: String {
        """
        group: \(name), sample: \(sample)
        user: \(user as Any)
        \(expdebug)
        -
        """
    }
    var expdebug: String {
        experiments.reduce(into: "") {
            $0 += "\($1)"
            $0 += "\n"
        }
    }
}

extension Groups: CustomStringConvertible {
    var description: String {
        """
        Groups:
        
        \(groupsdebug)
        """
    }
    var groupsdebug: String {
        groups.reduce(into: "") {
            $0 += "\($1)"
            $0 += "\n"
        }
    }
}

extension Dates: CustomStringConvertible {
    var description: String {
        "[start: \(start), end: \(end)]"
    }
}
