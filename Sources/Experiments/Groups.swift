/*
 Groups.swift
 */

struct Groups: Codable, Equatable {
    var groups: Set<Group>
    init(@GroupsBuilder _ makeGroups: () -> Set<Group>) {
        self.groups = makeGroups()
    }
    init(groups: Set<Group>) {
        self.groups = groups
    }
    static func - (lhs: Groups, rhs: Groups) -> Groups {
        let diff = lhs.groups.subtracting(rhs.groups)
        return Groups(groups: diff)
    }
    static func + (lhs: Groups, rhs: Groups) -> Groups {
        let diff = lhs.groups.union(rhs.groups)
        return Groups(groups: diff)
    }
}

public struct Group: Equatable, Hashable, Codable {
    var user: User?
    let name: String
    let sample: Double // 0...1
    var experiments: Set<Experiment>
    
    public init(name: String,
         sample: Double,
         @ExperimentsBuilder _ makeExperiments: () -> Set<Experiment> ) {
             self.user = nil
             self.name = name
             self.sample = sample / 100
             self.experiments = makeExperiments()
    }
    public init(name: String, experiments: [Experiment], sample: Int) {
        self.user = nil
        self.name = name
        self.sample = Double(sample) / 100
        self.experiments = Set(experiments)
    }
    public static func == (lhs: Group, rhs: Group) -> Bool {
        lhs.name == rhs.name
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
}

@resultBuilder
public struct ExperimentsBuilder {
    public static func buildBlock(_ components: Experiment...) -> Set<Experiment> {
        Set(components)
    }
}

@resultBuilder
public struct GroupsBuilder {
    public static func buildBlock(_ components: Group...) -> Set<Group> {
        Set(components)
    }
}
