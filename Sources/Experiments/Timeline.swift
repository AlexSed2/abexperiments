/*
 Timeline.swift
 Инстанс этой структуры будет получаться на финале инициализации библиотеки
 после всех шагов: получение массива экспериментов от юзера, склейки с инстансом с диска и расставление активных юзеров
 и будет харниться в памяти на протяжении лайфйайкла клиента.
 */


// Здесь будут расположены все эксперименты, которые когда либо были и будут
// Каждый эксперимент на своей дорожке
// Если инициализация происходит с экспериментом, который уже есть на диске,
// но с другим временем, то происходит ошибка (пока не сделано)
// Эксперименты заданные с сервера имеют приоритет над заданными с устройства

import Foundation

struct Timeline {
    
    let installDate: Date
    var user: Set<Experiment>
    var server: Set<Experiment>?
    let now: Date
    
    var experiments: Set<Experiment> {
        guard var server = server else { return user }
        user.forEach { experiment in
            if server.contains(where: { exist in
                exist.name == experiment.name
            }) == false {
                server.insert(experiment)
            }
        }
        return server
    }
    var current: [Experiment] {
        return experiments.filter {
            $0.dates.contains(now) &&
            $0.user != nil
        }
    }
    init(installDate: Date, now: Date?, groups: Groups) {
        self.installDate = installDate
        let experiments = groups.groups.reduce(into: Set<Experiment>()) {
            $0 = $0.union($1.experiments)
        }
        let stoplist = experiments.filter {
            $0.dates.start < installDate
        }
        self.user = experiments.subtracting(stoplist)
        self.now = now ?? Date()
    }
    mutating func uploadServer(groups: Groups) {
        let server = groups.groups.reduce(into: Set<Experiment>()) {
            $0 = $0.union($1.experiments)
        }
        let stoplist = server.filter {
            $0.dates.start < installDate
        }
        self.server = server.subtracting(stoplist)
    }
}


// API
extension Timeline {
    
    func isActive(experiment: Experiment) -> Bool {
        current.contains(experiment)
    }
    
    /// Активен ли сейчас эксперимент
    func isActive(nameExperiment: String) -> Bool {
        guard let experiment = experiments.first(where: {
            $0.name == nameExperiment
        }) else {
            return false
        }
        return isActive(experiment: experiment)
    }
    
    /// массив для юзера (все где учавствовал юзер)
    var allHaveBeenActive: [Experiment] {
        experiments.filter { experiment in
            guard experiment.user != nil else { return false }
            guard Date() >= experiment.dates.start else { return false }
            return true
        }
    }
    
    /// массив для суперпроперти (текущие активные)
    var superpProperty: [Experiment] {
        current
    }
    
}
