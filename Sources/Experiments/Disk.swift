import Files
import Foundation

struct DiskStorage {
    @StoreFile(target: .device) var deviceFile: File
    @StoreFile(target: .server) var serverFile: File
    func save(model: Groups, target: StoreFile.Target) throws {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        let data = try encoder.encode(model)
        switch target {
        case .server: try serverFile.write(data)
        case .device: try deviceFile.write(data)
        }
    }
    func load(target: StoreFile.Target) throws -> Groups {
        let file: File
        switch target {
        case .server: file = serverFile
        case .device: file = deviceFile
        }
        let decoder = JSONDecoder()
        let data = try file.read()
        let model = try decoder.decode(Groups.self, from: data)
        return model
    }
    func hardReset() throws {
        try deviceFile.delete()
        try serverFile.delete()
    }
}


@propertyWrapper
struct StoreFile {
    @StoreFolder var folder: Folder
    enum Target: String { case device = "Device", server = "Server"}
    let target: Target
    init() {
        target = .device
    }
    init(target: Target) {
        self.target = target
    }
    var filename: String { "Groups\(target.rawValue).json" }
    var wrappedValue: File {
        get {
            do {
                let file = try folder.file(named: filename)
                return file
            } catch {
                do {
                    let file = try folder.createFile(named: filename)
                    return file
                } catch {
                    fatalError("Experiments: could not create file")
                }
            }
        }
        set {
            fatalError("Experiments: attempt set file")
        }
    }
}


@propertyWrapper
struct StoreFolder {
    let foldername = "Experiments"
    var wrappedValue: Folder {
        get {
            if let folder = try? Folder.documents?.subfolder(named: foldername) {
                return folder
            } else {
                if let folder = try? Folder.documents?.createSubfolder(named: foldername) {
                    return folder
                } else {
                    fatalError("Experiments: could not create subfolder")
                }
            }
        }
        set {
            fatalError("Experiments: attempt set subfolder")
        }
    }
}
