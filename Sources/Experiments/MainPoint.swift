/*
 MainPoint.swift
 Воходная точка для пользователя библиотеки
 */

import Foundation


/// Библиотека A/Б тестов
public class ExperimentsLibrary {
        
    var timeline: Timeline
    let callback: (CallbackPacket) -> ()

    /// Оболочка для работы с А/Б тестами
    ///
    ///     let _ = Experiments {
    ///          Group(name: "Alpha", sample: 50) {
    ///             Experiment(name: "1", startDate: "21.01.2022", duration: 10)
    ///             Experiment(name: "2", startDate: "21.03.2022", duration: 10)
    ///          }
    ///          Group(name: "Beta", sample: 20) {
    ///              Experiment(name: "3", startDate: "21.01.2022", duration: 10)
    ///              Experiment(name: "4", startDate: "21.03.2022", duration: 10)
    ///          }
    ///      } callBack: { packet in
    ///             // to server: packet.user.map(\.name)
    ///             // to server: \(packet.superProperty.map(\.name))
    ///      }
    ///      // два массива, которые можно отправлять на сервер
    ///
    /// - Parameter installDate: Дата первого запуска приложения
    /// - Parameter makeGroups: Группы экспериментов
    /// - Parameter callBack: Вызывается сразу после инициализации и отдаёт массивы на аналитику
    ///   user: массивы всех когда либо активных экспериментов, в которых участвовал юзер, для отправки на юзера
    ///   superpProperty: массив текущих активных экспериментов для супер свойства
    ///
    ///
    public init(installDate: Date = .distantPast, testNow: Date? = nil, @GroupsBuilder _ makeGroups: () -> Set<Group>, callBack: @escaping (CallbackPacket) -> ()) {
        let incomGroups = Groups(groups: makeGroups())
        do {
            let storedGroups = try DiskStorage().load(target: .device)
            let setuped = GroupSetup().setupGroups(incomGroups: incomGroups, storedGroups: storedGroups)
            timeline = Timeline(installDate: installDate, now: testNow, groups: setuped)
            try? DiskStorage().save(model: setuped, target: .device)
        } catch {
            let setuped = Setup().fullfill(groups: incomGroups)
            timeline = Timeline(installDate: installDate, now: testNow, groups: setuped)
            try? DiskStorage().save(model: setuped, target: .device)
        }
        do {
            let storedGroups = try DiskStorage().load(target: .server)
            timeline.uploadServer(groups: storedGroups)
        } catch {}
        self.callback = callBack
        callBack(CallbackPacket(user: timeline.allHaveBeenActive, superProperty: timeline.superpProperty))
        ExperimentsLibrary.shared = self
    }
    
    // API

    /// Задаёт експерименты с сервера и вызывает снова колбек, эксперименты заданные с сервера имеют приоритет над заданными с устройства
    public func set(serverExperiments: Set<Group>) {
        let incomGroups = Groups(groups: serverExperiments)
        do {
            let storedGroups = try DiskStorage().load(target: .server)
            let setuped = GroupSetup().setupGroups(incomGroups: incomGroups, storedGroups: storedGroups)
            timeline.uploadServer(groups: setuped)
            try? DiskStorage().save(model: setuped, target: .server)
        } catch {
            let setuped = Setup().fullfill(groups: incomGroups)
            timeline.uploadServer(groups: setuped)
            try? DiskStorage().save(model: setuped, target: .server)
        }
        callback(CallbackPacket(user: timeline.allHaveBeenActive, superProperty: timeline.superpProperty))
    }
    
    public func isActive(nameExperiment: String) -> Bool {
        timeline.isActive(nameExperiment: nameExperiment)
    }
}

extension ExperimentsLibrary {
    public static var shared: ExperimentsLibrary!
}

// API
public struct CallbackPacket {
    public let user: [Experiment]
    public let superProperty: [Experiment]
}
