// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Experiments",
    products: [
        .library(
            name: "Experiments",
            targets: ["Experiments"]),
    ],
    dependencies: [
        .package(url: "https://github.com/JohnSundell/Files", from: "4.0.0"),
    ],
    targets: [
        .target(
            name: "Experiments",
            dependencies: ["Files"]),
        .executableTarget(
            name: "Runner",
            dependencies: ["Experiments"]),
        .testTarget(
            name: "ExperimentsTests",
            dependencies: ["Experiments"]),
    ]
)
